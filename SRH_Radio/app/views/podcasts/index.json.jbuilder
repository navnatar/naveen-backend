json.array!(@podcasts) do |podcast|
  json.extract! podcast, :id, :podcastName, :category, :subcategory, :description
  json.url podcast_url(podcast, format: :json)
end
