class CreatePodcasts < ActiveRecord::Migration
  def change
    create_table :podcasts do |t|
      t.string :podcastName
      t.string :category
      t.string :subcategory
      t.string :description

      t.timestamps
    end
  end
end
